import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Profile from "@/views/Profile";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile
  },
  {
    path: '/student',
    name: 'Student',
    component: () => import('../views/Student/ViewStudent')
  },
  {
    path: '/student/create',
    name: 'StudentForm',
    component: () => import('../views/Student/FormStudent')
  },
  {
    path: '/student/:id',
    name: 'StudentEdit',
    component: () => import('../views/Student/FormStudent')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
