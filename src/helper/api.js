import axios from 'axios'

const baseUrl = 'http://localhost/belajar-laravel/public/api'

const api = axios.create({
  baseURL: baseUrl
})

export default api